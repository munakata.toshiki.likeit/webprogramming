
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<title style="margin: 20px">ユーザー一覧</title>
</head>
<body>
	<div align="center">
		<div class="alert alert-dark">
			<div class="row" style="margin-left: 400px">
				<div style="margin-right: 100px">${userInfo.name}さん</div>
				<a style="font-size: 20px; color: #ff3300;" href="LogOutServlet"><u>ログアウト</u></a>
			</div>
		</div>
		<div class="container">
			<h2>ユーザー一覧</h2>
			<div style="margin-left: 1000px">
				<a href="UserCreateServlet"><u>新規登録</u></a>
			</div>
			<form method="post" action="UserListServlet">
				<div align="center">

					<div class="form-group row col-6 ">
						<div style="margin-right: 130px">
							<label>ログインID</label>
						</div>
						<input type="text" name="loginId" id="loginId">
					</div>

					<div class="form-group row col-6 ">
						<div style="margin-right: 130px">
							<label>ユーザー名</label>
						</div>
						<input type="text" name="user_name" id="user_name">
					</div>
					<div class="form-group row col-6">
						<label>生年月日</label>
						<div class="col">
							<input type="date" name="date_start" id="date_start"
								placeholder="年/月/日">
						</div>
						<div>〜</div>
						<div class="col">
							<input type="date" name="date_end" id="date_end"
							placeholder="年/月/日">
						</div>
					</div>
				</div>
				<button style="margin-left: 1000px" value="検索" type="submit">
					検索</button>
			</form>
			<hr>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ログインID</th>
							<th>ユーザ名</th>
							<th>生年月日</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${userList}">
							<tr>
								<td>${user.loginId}</td>
								<td>${user.name}</td>
								<td>${user.birthDate}</td>
								<!-- TODO 未実装；ログインボタンの表示制御を行う -->


								<td>




								<a class="btn btn-primary"
									href="UserDetailServlet?id=${user.id}">詳細</a>

								<c:choose >
								<c:when test="${userInfo.loginId == 'admin'}">
								<a class="btn btn-success"
									href="UpdateServlet?id=${user.id}">更新</a>
									</c:when>

									<c:when test="${userInfo.loginId == user.loginId }">
									<a class="btn btn-success"
									href="UpdateServlet?id=${user.id}">更新</a>
									</c:when>
									</c:choose>

									<c:if test ="${userInfo.loginId == 'admin'}">
									<a class="btn btn-danger"
									href="UserDeleteServlet?id=${user.id}">削除</a></td>
									</c:if>



							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
