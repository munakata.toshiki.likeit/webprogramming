<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<title>ユーザー削除確認</title>
</head>
<body>
	<div align=center>

		<div class="alert alert-dark">
			<div class="row" style="margin-left: 400px">
				<div style="margin-right: 100px">${userInfo.name}さん</div>
				<a style="font-size: 20px; color: #ff3300;" href="LogOutServlet"><u>ログアウト</u></a>
			</div>
		</div>
		<h2>ユーザー削除確認</h2>


		<br>

		<div>
			ログインID : ${user.loginIdData} <br>を本当に削除してよろしいでしょうか。
		</div>
		<br> <br>




		<div class="mx-auto">

			<form action="UserDeleteServlet" method="post">
				<input type="hidden" name="loginId" value="${user.loginIdData}">


				<a href="UserListServlet">
					<button style="margin: 15px" type="button">キャンセル</button>
				</a>

				<button style="margin: 15px" type="submit">OK</button>
			</form>
		</div>
	</div>



</body>
</html>
