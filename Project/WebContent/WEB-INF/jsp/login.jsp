<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<title>ログイン画面</title>
</head>
<body>
	<div align="center">
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<h3 style="margin: 40px">ログイン画面</h3>
		<div class="mr-5">
			<form class="form-signin" action="LoginServlet" method="post">
				<div class="form-group row col-4">
					<label class="mr-5">ログインID</label> <input type="text"
						name="loginId">
				</div>
				<div class="form-group row col-4">
					<label class="mr-5" for="exampleInputPassword1">パスワード</label> <input
						type="password" name="password" id="exampleInputPassword1">
				</div>
				<button style="margin: 30px" type="submit">ログイン</button>
			</form>
		</div>
	</div>
</body>
</html>
