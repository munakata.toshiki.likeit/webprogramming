
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
    crossorigin="anonymous">
	<title>ユーザー新規登録</title>
</head>
	<body>
                <div align="center">


         <div class="alert alert-dark">
         <div class="row" style="margin-left: 400px">
         <div style="margin-right: 100px">${userInfo.name}さん</div> <a style="font-size: 20px;
          color: #ff3300;"
         href="LogOutServlet"><u>ログアウト</u></a>
         </div>
         </div>
        <h2>ユーザー新規登録</h2>
        <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" style= "color:#ff3300;" role="alert">
		  ${errMsg}
		</div>
	</c:if>

          <form method="post" action="UserCreateServlet">
         <div class="form-group row col-4">
             <div class="mr-5"><label >ログインID</label></div>
             <div><input  type="text" name="loginId" id="loginId" ></div>
          </div>

  <div class="form-group row col-4">
    <label for="exampleInputPassword1" class="mr-5">パスワード</label>
    <input type="password"  id="exampleInputPassword1" name="password">
  </div>

  <div class="form-group row col-4">
    <label for="exampleInputPassword1" class="mr-2">パスワード(確認)</label>
    <input type="password"  id="exampleInputPassword1" name="password2">
  </div>

   <div class="form-group row col-4 ">
    <label class="mr-5">ユーザー名</label>
<input  type="text" name="name" id="name" ></div>

             <div class="form-group row col-4 ">
    <label class="mr-5">生年月日　</label>
<input  type="date" name="birthDate" id="birthDate"></div>

             <button type="submit" >　登録　</button>

       <div  style="margin-left: 370px"align="left"><a
             href="UserListServlet" ><u>戻る</u></a></div>

             </form>
        </div>
	</body>
</html>
