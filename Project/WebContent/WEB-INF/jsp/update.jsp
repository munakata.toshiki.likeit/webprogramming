<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<title>ユーザー情報更新</title>
</head>
	<body>

        <div align="center">

           <div class="alert alert-dark">
         <div class="row" style="margin-left: 400px">
         <div style="margin-right: 100px">${userInfo.name}さん</div> <a style="font-size: 20px; color: #ff3300;" href="LogOutServlet"><u>ログアウト</u></a>
         </div>
         </div>
        <h2  style="margin:40px ">ユーザー情報更新</h2>
        <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" style= "color:#ff3300;" role="alert">
		  ${errMsg}
		</div>
	</c:if>


         <form method="post" action="UpdateServlet">
         <input type="hidden" name="loginId" value="${user.loginIdData}">

 <div class="form-group row col-4">
 ログインID 　　　　　　${user.loginIdData}</div>



  <div  style="margin: 20px"class="form-group row col-4">
    <div class="mr-5">
      <label for="exampleInputPassword1">パスワード</label>
      </div>
    <input type="password"  id="exampleInputPassword1" name="password">
  </div>

  <div class="form-group row col-4"><div class="mr-1">
      <label for="exampleInputPassword1">パスワード(確認)</label></div>
    <input type="password"  id="exampleInputPassword1" name="password2">
  </div>


     <div class="form-group row col-4">
         <div class="mr-5">
             <label >ユーザー名</label></div>
<input  type="text"  name = "name"  value= ${user.nameData}>
              </div>

     <div class="form-group row col-4">
         <div class="mr-5">
             <label >生年月日 　</label></div>
<input  type="text"  name = "birth_date"  value=${user.birthDate}>
           </div>


  <button type="submit" >　更新　</button>

             </form>

             </div>

             <div  style="margin-left: 400px"align="left"><a
             href="UserListServlet" ><u>戻る</u></a></div>



	</body>
</html>
