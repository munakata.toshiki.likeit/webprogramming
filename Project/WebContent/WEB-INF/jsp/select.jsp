<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<title>ユーザー情報詳細参照</title>
</head>
    <body>
            <div align="center">


       <div class="alert alert-dark">
         <div class="row" style="margin-left: 400px">
         <div style="margin-right: 100px">${userInfo.name}さん</div> <a style="font-size: 20px; color: #ff3300;" href="LogOutServlet"><u>ログアウト</u></a>
         </div>
         </div>
        <h2 style="margin: 40px">ユーザー情報詳細参照</h2>


         <div style="margin: 10px">ログインID 　　　　　　${user.loginIdData}</div>
         <div style="margin: 10px">ユーザー名 　　　　　　${user.nameData}</div>
         <div style="margin: 10px">生年月日 　　　　　　${user.birthDate}</div>
         <div style="margin: 10px">登録日時 　　　　　　${user.createDate}</div>
         <div style="margin: 10px">更新日時 　　　　　　${user.updateDate}</div>

        <br>
        </div>
            <div  style="margin-left: 400px"align="left"><a
             href="UserListServlet" ><u>戻る</u></a></div>
	</body>
</html>
